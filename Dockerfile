# This file is a template, and might need editing before it works on your project.
FROM rust:1.45 as builder

WORKDIR /usr/src/gabbert_sabbert_bot

COPY . .
RUN cargo build --release

FROM debian:buster-slim

COPY --from=builder /usr/src/gabbert_sabbert_bot/target/release/gabbert_sabbert_bot .

CMD ["./gabbert_sabbert_bot"]
