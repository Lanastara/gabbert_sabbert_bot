use std::{
    path::Path,
    sync::{Arc, RwLock},
};

use chrono::{DateTime, Utc};
use pickledb::PickleDb;
use serenity::{
    model::id::{ChannelId, MessageId, UserId},
    prelude::TypeMapKey,
};
use tracing::info;

#[derive(Clone)]
pub struct Database {
    db: Arc<RwLock<PickleDb>>,
}

impl Database {
    pub fn new(path: &Path) -> Self {
        Self {
            db: Arc::new(RwLock::new(
                PickleDb::load(
                    path,
                    pickledb::PickleDbDumpPolicy::AutoDump,
                    pickledb::SerializationMethod::Json,
                )
                .unwrap_or(PickleDb::new(
                    path,
                    pickledb::PickleDbDumpPolicy::AutoDump,
                    pickledb::SerializationMethod::Json,
                )),
            )),
        }
    }

    fn get_message_key(message: MessageId) -> String {
        format!("post-{}", message.as_u64())
    }

    fn get_user_flagging_key(user: UserId, channel: ChannelId) -> String {
        format!("flags-{}-{}", user.as_u64(), channel.as_u64())
    }

    fn get_user_ban_key(user: UserId, channel: ChannelId) -> String {
        format!("ban-{}-{}", user.as_u64(), channel.as_u64())
    }

    pub fn flag_post(&self, message: MessageId) -> Result<u8, ()> {
        info!("Flagging Post: {}", message);
        let mut db = self.db.write().unwrap();

        let message_key = Self::get_message_key(message);

        let no_flags: Option<u8> = db.get(&message_key);

        let new_no_flags: u8 = match no_flags {
            Some(flags) => flags + 1,
            None => 1,
        };

        db.set(&message_key, &new_no_flags).unwrap();

        Ok(new_no_flags)
    }

    pub fn unflag_post(&self, message: MessageId) -> Result<(), ()> {
        info!("Unflagging Post: {}", message);

        let mut db = self.db.write().unwrap();

        let message_key = Self::get_message_key(message);

        let no_flags: Option<u8> = db.get(&message_key);

        let new_no_flags: u8 = match no_flags {
            Some(0) => 0,
            Some(flags) => flags - 1,
            None => 0,
        };

        if new_no_flags == 0 {
            db.rem(&message_key).unwrap();
        } else {
            db.set(&message_key, &new_no_flags).unwrap();
        }

        Ok(())
    }

    pub fn delete_post_flags(&self, message: MessageId) -> Result<(), ()> {
        info!("Deleting Post from DB: {}", message);

        let mut db = self.db.write().unwrap();

        let message_key = Self::get_message_key(message);

        db.rem(&message_key).unwrap();

        Ok(())
    }

    pub fn flag_user(&self, user: UserId, channel: ChannelId) -> Result<u8, ()> {
        info!("Flagging User: {}", user);
        let mut db = self.db.write().unwrap();

        let key = Self::get_user_flagging_key(user, channel);

        let no_flags: Option<u8> = db.get(&key);

        let new_no_flags: u8 = match no_flags {
            Some(flags) => flags + 1,
            None => 1,
        };

        db.set(&key, &new_no_flags).unwrap();
        Ok(new_no_flags)
    }

    pub fn clear_user_flags(&self, user: UserId, channel: ChannelId) -> Result<(), ()> {
        info!("Clearing user Flags for Channel: {}, {}", user, channel);
        let mut db = self.db.write().unwrap();

        let key = Self::get_user_flagging_key(user, channel);

        db.rem(&key).unwrap();

        Ok(())
    }

    pub fn save_user_ban(
        &self,
        user: UserId,
        channel: ChannelId,
        until: DateTime<Utc>,
    ) -> Result<(), ()> {
        info!("Saving User Ban: {}", user);
        let mut db = self.db.write().unwrap();

        let key = Self::get_user_ban_key(user, channel);

        db.set(&key, &until).unwrap();
        Ok(())
    }

    pub fn list_user_bans(&self) -> Vec<(UserId, ChannelId, DateTime<Utc>)> {
        let db = self.db.read().unwrap();

        db.iter()
            .filter_map(|kv| {
                let key = kv.get_key();
                if key.starts_with("ban") {
                    let parts: Vec<_> = key.split("-").collect();
                    let uid: Option<u64> = parts[1].parse().ok();
                    let cid: Option<u64> = parts[2].parse().ok();

                    if let (Some(uid), Some(cid)) = (uid, cid) {
                        let value = kv.get_value::<DateTime<Utc>>()?;

                        Some((UserId::from(uid), ChannelId::from(cid), value))
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn remove_user_ban(&self, user: UserId, channel: ChannelId) -> Result<(), ()> {
        info!("Deleting User Ban: {}", user);

        let mut db = self.db.write().unwrap();

        let key = Self::get_user_ban_key(user, channel);

        db.rem(&key).unwrap();

        Ok(())
    }
}

impl TypeMapKey for Database {
    type Value = Database;
}
