mod commands;
mod db;

use chrono::{Duration, Utc};
use chrono_humanize::{Accuracy, Tense};
use db::Database;
use serde_json::Value;
use serenity::{
    async_trait,
    builder::CreateMessage,
    client::bridge::gateway::{GatewayIntents, ShardManager},
    collector::message_collector,
    framework::{standard::macros::group, StandardFramework},
    http::Http,
    model::{
        channel::{Message, PermissionOverwrite, PermissionOverwriteType, Reaction, ReactionType},
        event::ResumedEvent,
        gateway::Ready,
        id::ChannelId,
        Permissions,
    },
    prelude::*,
    utils,
};
use std::{collections::HashSet, env, path::Path, sync::Arc};

use tracing::{error, info};
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use commands::*;

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}

fn copy_message<'a, 'b>(
    builder: &'a mut CreateMessage<'b>,
    message: &Message,
    prefix: &str,
) -> &'a mut CreateMessage<'b> {
    builder.content(format!("{}\r\n{}", prefix, message.content.clone()));

    for e in &message.embeds {
        builder.embed(|b| {
            // if let Some(attachment) = e.{
            //     b.attachment(attachment);
            // }

            if let Some(author) = &e.author {
                b.author(|f| {
                    if let Some(url) = &author.url {
                        f.url(url);
                    }

                    f.name(author.name.clone());

                    if let Some(icon_url) = &author.icon_url {
                        f.icon_url(icon_url);
                    }

                    f
                });
            }

            b.color(e.colour);

            if let Some(description) = &e.description {
                b.description(description);
            }

            for field in &e.fields {
                b.field(field.name.clone(), field.value.clone(), field.inline);
            }

            if let Some(footer) = &e.footer {
                b.footer(|f| {
                    if let Some(icon_url) = &footer.icon_url {
                        f.icon_url(icon_url);
                    }
                    f.text(footer.text.clone());
                    f
                });
            }

            if let Some(image) = &e.image {
                b.image(image.url.clone());
            }

            if let Some(thumbnail) = &e.thumbnail {
                b.thumbnail(thumbnail.url.clone());
            }

            if let Some(timestamp) = &e.timestamp {
                b.timestamp(timestamp.clone());
            }

            if let Some(title) = &e.title {
                b.title(title);
            }

            if let Some(url) = &e.url {
                b.url(url);
            }

            b
        });
    }

    builder
}

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn reaction_add(&self, ctx: Context, ev: Reaction) {
        let message = ev.message(&ctx.http).await;
        let data = ctx.data.read().await;
        let settings = data.get::<Settings>();
        let db = data.get::<Database>();

        if let (Some(db), Some(settings)) = (db, settings) {
            let post_flags = match ev.emoji {
                ReactionType::Custom {
                    name: Some(name), ..
                } if name == "notok" => db.flag_post(ev.message_id).unwrap(),
                _ => 0,
            };

            if (post_flags >= settings.max_post_flags) {
                if let Ok(message) = message {
                    let message_clone = message.clone();
                    let author = message.author.id;
                    println!("deleting message");

                    ctx.http
                        .delete_message(*message.channel_id.as_u64(), *message.id.as_u64())
                        .await
                        .unwrap();
                    // message.delete(&ctx).await.unwrap();

                    db.delete_post_flags(ev.message_id).unwrap();
                    println!("message deleted");

                    let user_flags = db.flag_user(author, message.channel_id).unwrap();

                    let mut b = CreateMessage::default();
                    copy_message(
                        &mut b,
                        &message_clone,
                        &format!("Message was deleted becauce it was deemed inappropriate.\n Author: <@{}>, Channel: <#{}>", message_clone.author.id.0, message_clone.channel_id.0),
                    );

                    let map = utils::hashmap_to_json_map(b.0.clone());

                    if let Err(_) = ctx
                        .http
                        .send_message(settings.trashfire_channel.0, &Value::Object(map))
                        .await
                    {}

                    if let Err(_) = message_clone.author.direct_message(&ctx, |b| {
                        copy_message(
                            b,
                            &message_clone,
                            &format!("Message was deleted becauce it was deemed inappropriate.\n{} more and you will be stopped from posting for {}", settings.max_user_flags - user_flags, chrono_humanize::HumanTime::from(settings.channel_timeout).to_text_en(Accuracy::Precise, Tense::Present)),
                        )
                    })
                    .await
                {}

                    if user_flags >= settings.max_user_flags {
                        let overwrite = PermissionOverwrite {
                            allow: Permissions::empty(),
                            deny: Permissions::SEND_MESSAGES,
                            kind: PermissionOverwriteType::Member(author),
                        };

                        println!("stopping user");

                        db.save_user_ban(
                            author,
                            message.channel_id,
                            message.timestamp + settings.channel_timeout,
                        )
                        .unwrap();

                        ev.channel_id
                            .create_permission(&ctx.http, &overwrite)
                            .await
                            .unwrap();

                        db.clear_user_flags(author, message.channel_id).unwrap();
                    }
                }
            }
        }
    }

    async fn reaction_remove(&self, ctx: Context, ev: Reaction) {
        match ev.emoji {
            ReactionType::Custom {
                name: Some(name), ..
            } if name == "notok" => {
                let data = ctx.data.read().await;
                let db = data.get::<Database>();
                if let Some(db) = db {
                    db.unflag_post(ev.message_id).unwrap();
                }
            }
            _ => {}
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Resumed");
    }
}

#[group]
#[commands(ping)]
struct General;

struct Settings {
    channel_timeout: Duration,
    max_post_flags: u8,
    max_user_flags: u8,
    trashfire_channel: ChannelId,
}

impl Settings {
    fn from_env() -> Self {
        let max_post_flags: u8 = env::var("GSB_FLAGS_FOR_MESSAGE")
            .ok()
            .map(|s| s.parse().ok())
            .flatten()
            .unwrap_or(3);

        let max_user_flags: u8 = env::var("GSB_FLAGS_FOR_MEMBER")
            .ok()
            .map(|s| s.parse().ok())
            .flatten()
            .unwrap_or(3);

        let channel_timeout: Duration = Duration::milliseconds(
            env::var("GSB_MEMBER_CHANNEL_TIMEOUT")
                .ok()
                .map(|s| s.parse().ok())
                .flatten()
                .unwrap_or(3600),
        );

        let trashfire_channel: ChannelId = ChannelId(
            env::var("GSB_TRASHFIRE_CHANNEL")
                .ok()
                .map(|s| s.parse().ok())
                .flatten()
                .unwrap_or(3600),
        );

        Self {
            max_post_flags,
            max_user_flags,
            channel_timeout,
            trashfire_channel,
        }
    }
}

impl TypeMapKey for Settings {
    type Value = Settings;
}

#[tokio::main]
async fn main() {
    // This will load the environment variables located at `./.env`, relative to
    // the CWD. See `./.env.example` for an example on how to structure this.
    dotenv::dotenv().expect("Failed to load .env file");

    let settings = Settings::from_env();

    let db_path = env::var("GSB_DB_DIR").unwrap_or(format!("bot.db"));
    let db_path: &Path = Path::new(&db_path);

    // Initialize the logger to use environment variables.
    //
    // In this case, a good default is setting the environment variable
    // `RUST_LOG` to debug`.
    let subscriber = FmtSubscriber::builder()
        .with_env_filter(EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("Failed to start the logger");

    let token = env::var("GSB_DISCORD_TOKEN").expect("Expected a token in the environment");

    let http = Http::new_with_token(&token);

    // We will fetch your bot's owners and id
    let (owners, _bot_id) = match http.get_current_application_info().await {
        Ok(info) => {
            let mut owners = HashSet::new();
            owners.insert(info.owner.id);

            (owners, info.id)
        }
        Err(why) => panic!("Could not access application info: {:?}", why),
    };

    // Create the framework
    let framework = StandardFramework::new()
        .configure(|c| c.owners(owners).prefix("~"))
        .group(&GENERAL_GROUP);

    let mut client = Client::builder(&token)
        .intents(
            GatewayIntents::GUILD_PRESENCES
                | GatewayIntents::GUILD_VOICE_STATES
                | GatewayIntents::GUILD_MESSAGES
                | GatewayIntents::GUILD_MESSAGE_REACTIONS
                | GatewayIntents::DIRECT_MESSAGE_REACTIONS
                | GatewayIntents::DIRECT_MESSAGES,
        )
        .framework(framework)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    let db = Database::new(db_path);

    {
        let mut data = client.data.write().await;
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<Database>(db.clone());
        data.insert::<Settings>(settings);
    }

    let shard_manager = client.shard_manager.clone();

    tokio::spawn(async move {
        tokio::signal::ctrl_c()
            .await
            .expect("Could not register ctrl+c handler");
        shard_manager.lock().await.shutdown_all().await;
    });

    let worker = tokio::spawn(async move {
        let mut interval = tokio::time::interval(tokio::time::Duration::from_secs(30));

        loop {
            interval.tick().await;

            let bans = db.list_user_bans();

            for (user, channel, _) in bans.into_iter().filter(|(_, _, date)| *date <= Utc::now()) {
                channel
                    .delete_permission(&http, PermissionOverwriteType::Member(user))
                    .await
                    .unwrap();

                db.remove_user_ban(user, channel).unwrap();
            }
        }
    });

    if let Err(why) = client.start().await {
        error!("Client error: {:?}", why);
    }

    worker.abort();
}
